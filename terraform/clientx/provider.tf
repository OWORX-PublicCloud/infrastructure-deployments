provider "azurerm" {
  features {}

}
terraform {
  required_version = ">=0.15"
  required_providers {
      azurerm = {
          source = "hashicorp/azurerm"
          version = "2.73.0"
      }
  }
  backend "azurerm" {
    storage_account_name = "oworxterraform"
    container_name       = "tfstate"
    key                  = "prod.terraform.tfstate"
  }
}