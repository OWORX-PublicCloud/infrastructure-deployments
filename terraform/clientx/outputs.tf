## outputs the Vnet ID, can be used for peering or a vHub connection ##

output "vnet-id" {
  value = azurerm_virtual_network.example.id
}