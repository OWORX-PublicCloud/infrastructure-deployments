## creates the VNET with 2 subnets ##

resource "azurerm_virtual_network" "example" {
  address_space = var.vnet-cidr
  location = var.location
  name = "${var.region-code}-${var.vnet-name}"
  resource_group_name = var.rg

subnet {
  address_prefix = var.sub1-cidr
  name = "${var.region-code}-${var.vnet-name}-${var.environment}-${var.workload}-1"
}
subnet {
  address_prefix = var.sub2-cidr
  name = "${var.region-code}-${var.vnet-name}-${var.environment}-${var.workload}-2"
}
}
