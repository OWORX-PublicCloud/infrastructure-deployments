variable "vnet-cidr" {
  type = list(string)
  description = "vnet subnet range"
}

variable "location" {
  type = string
  description = "vnet location"
}

variable "rg" {
  type = string
  description = "resource group name"
}

variable "vnet-name" {
  type = string
  description = "name of the vnet"
}

variable "sub1-cidr" {
  type = string
  description = "subnet 1 subnet range"
}

variable "sub2-cidr" {
  type = string
  description = "subnet 2 subnet range"
}

variable "region-code" {
  type = string
  description = "the Region code as in UKS"
}

variable "environment" {
  type = string
  description = "environment as in Production, DMZ etc."
}

variable "workload" {
  type = string
  description = "workload, for example dmz, ads"
}